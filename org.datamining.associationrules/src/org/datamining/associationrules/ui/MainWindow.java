/**
 * Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.    
 * In no event will the author(s) be held liable for any damages arising from the use of this software.  
 * 
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org) and Hazem El-Raffiee (hazem.farouk.elraffiee@gmail.com)  
 * 
 * All of the files that are part of the GCDs Association Rules algorithm are licensed under either GPL v.3 or dual-licensed under the following terms.  
 * 
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).  
 * 2- The license text must be kept in source files headers.   
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents.   
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/gcd-association-rules)  
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log.   
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).  
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).  
 * 6- For commercial distribution and use, a license agreement must be obtained from one of the author(s).  
*/

package org.datamining.associationrules.ui;

import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileFilter;

import org.datamining.associationrules.GCDAssociationRules;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {
	private DropableTextField txtInput;
	private DropableTextField txtOutput;
	private JTextArea display;
	private JSpinner spinnerSupport;
	private JButton btnRun;
	private JProgressBar progressBar;
	private JLabel progressLabel;

	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			GCDAssociationRules.main(args);
			return;
		}
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e1) {
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		super("GCD Association Rules Algorithm");
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(MainWindow.class.getResource("/org/datamining/associationrules/ui/icon.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(622, 393);
		setLocationRelativeTo(null);

		JPanel gcdAssociationPanel = new JPanel();
		gcdAssociationPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(gcdAssociationPanel);
		GridBagLayout gbl_gcdAssociationTab = new GridBagLayout();
		gbl_gcdAssociationTab.columnWidths = new int[] { 20, 0, 0, 0, 0, 20, 0 };
		gbl_gcdAssociationTab.rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_gcdAssociationTab.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_gcdAssociationTab.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gcdAssociationPanel.setLayout(gbl_gcdAssociationTab);

		JLabel lblDatasetFile = new JLabel("Dataset file:");
		GridBagConstraints gbc_lblDatasetFile = new GridBagConstraints();
		gbc_lblDatasetFile.anchor = GridBagConstraints.WEST;
		gbc_lblDatasetFile.insets = new Insets(0, 0, 5, 5);
		gbc_lblDatasetFile.gridx = 1;
		gbc_lblDatasetFile.gridy = 1;
		gcdAssociationPanel.add(lblDatasetFile, gbc_lblDatasetFile);

		txtInput = new DropableTextField();
		GridBagConstraints gbc_txtInput = new GridBagConstraints();
		gbc_txtInput.insets = new Insets(0, 0, 5, 5);
		gbc_txtInput.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtInput.gridx = 2;
		gbc_txtInput.gridy = 1;
		gcdAssociationPanel.add(txtInput, gbc_txtInput);
		txtInput.setColumns(10);

		JButton btnBrowse_1 = new JButton("Browse");
		btnBrowse_1.addActionListener(new BrowseActionListener(txtInput, "*.dat", ".dat"));
		GridBagConstraints gbc_btnBrowse_1 = new GridBagConstraints();
		gbc_btnBrowse_1.gridwidth = 2;
		gbc_btnBrowse_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnBrowse_1.insets = new Insets(0, 0, 5, 5);
		gbc_btnBrowse_1.gridx = 3;
		gbc_btnBrowse_1.gridy = 1;
		gcdAssociationPanel.add(btnBrowse_1, gbc_btnBrowse_1);

		JLabel lblOutputPath_1 = new JLabel("Output path:");
		GridBagConstraints gbc_lblOutputPath_1 = new GridBagConstraints();
		gbc_lblOutputPath_1.anchor = GridBagConstraints.WEST;
		gbc_lblOutputPath_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblOutputPath_1.gridx = 1;
		gbc_lblOutputPath_1.gridy = 2;
		gcdAssociationPanel.add(lblOutputPath_1, gbc_lblOutputPath_1);

		txtOutput = new DropableTextField();
		GridBagConstraints gbc_txtOutput = new GridBagConstraints();
		gbc_txtOutput.insets = new Insets(0, 0, 5, 5);
		gbc_txtOutput.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOutput.gridx = 2;
		gbc_txtOutput.gridy = 2;
		gcdAssociationPanel.add(txtOutput, gbc_txtOutput);
		txtOutput.setColumns(10);

		JButton button = new JButton("Browse");
		button.addActionListener(new BrowseActionListener(txtOutput, "*.csv", ".csv"));
		GridBagConstraints gbc_button = new GridBagConstraints();
		gbc_button.gridwidth = 2;
		gbc_button.fill = GridBagConstraints.HORIZONTAL;
		gbc_button.insets = new Insets(0, 0, 5, 5);
		gbc_button.gridx = 3;
		gbc_button.gridy = 2;
		gcdAssociationPanel.add(button, gbc_button);

		JLabel lblMinimumFrequency = new JLabel("Minimum Support:");
		GridBagConstraints gbc_lblMinimumFrequency = new GridBagConstraints();
		gbc_lblMinimumFrequency.anchor = GridBagConstraints.WEST;
		gbc_lblMinimumFrequency.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinimumFrequency.gridx = 1;
		gbc_lblMinimumFrequency.gridy = 4;
		gcdAssociationPanel.add(lblMinimumFrequency, gbc_lblMinimumFrequency);

		spinnerSupport = new JSpinner();
		spinnerSupport.setModel(new SpinnerNumberModel(0.0, 0.0, 1.0, 0.01));
		GridBagConstraints gbc_spinnerSupport = new GridBagConstraints();
		gbc_spinnerSupport.gridwidth = 3;
		gbc_spinnerSupport.fill = GridBagConstraints.BOTH;
		gbc_spinnerSupport.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerSupport.gridx = 2;
		gbc_spinnerSupport.gridy = 4;
		gcdAssociationPanel.add(spinnerSupport, gbc_spinnerSupport);

		lblMinimumConfidence = new JLabel("Minimum Confidence:");
		GridBagConstraints gbc_lblMinimumConfidence = new GridBagConstraints();
		gbc_lblMinimumConfidence.anchor = GridBagConstraints.WEST;
		gbc_lblMinimumConfidence.insets = new Insets(0, 0, 5, 5);
		gbc_lblMinimumConfidence.gridx = 1;
		gbc_lblMinimumConfidence.gridy = 5;
		gcdAssociationPanel.add(lblMinimumConfidence, gbc_lblMinimumConfidence);

		spinnerConfidence = new JSpinner();
		spinnerConfidence.setModel(new SpinnerNumberModel(0.0, 0.0, 1.0, 0.01));
		GridBagConstraints gbc_spinnerConfidence = new GridBagConstraints();
		gbc_spinnerConfidence.fill = GridBagConstraints.BOTH;
		gbc_spinnerConfidence.gridwidth = 3;
		gbc_spinnerConfidence.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerConfidence.gridx = 2;
		gbc_spinnerConfidence.gridy = 5;
		gcdAssociationPanel.add(spinnerConfidence, gbc_spinnerConfidence);

		lblMaximumCombinationsLimit = new JLabel("Maximum Combinations:");
		GridBagConstraints gbc_lblMaximumCombinationsLimit = new GridBagConstraints();
		gbc_lblMaximumCombinationsLimit.anchor = GridBagConstraints.WEST;
		gbc_lblMaximumCombinationsLimit.insets = new Insets(0, 0, 5, 5);
		gbc_lblMaximumCombinationsLimit.gridx = 1;
		gbc_lblMaximumCombinationsLimit.gridy = 6;
		gcdAssociationPanel.add(lblMaximumCombinationsLimit, gbc_lblMaximumCombinationsLimit);

		spinnerCombinationsLimit = new JSpinner();
		spinnerCombinationsLimit.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spinnerCombinationsLimit = new GridBagConstraints();
		gbc_spinnerCombinationsLimit.fill = GridBagConstraints.HORIZONTAL;
		gbc_spinnerCombinationsLimit.gridwidth = 3;
		gbc_spinnerCombinationsLimit.insets = new Insets(0, 0, 5, 5);
		gbc_spinnerCombinationsLimit.gridx = 2;
		gbc_spinnerCombinationsLimit.gridy = 6;
		gcdAssociationPanel.add(spinnerCombinationsLimit, gbc_spinnerCombinationsLimit);

		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 8;
		gcdAssociationPanel.add(scrollPane, gbc_scrollPane);

		display = new JTextArea();
		scrollPane.setViewportView(display);
		display.setEditable(false);
		display.setFont(UIManager.getFont("TextField.font"));
		display.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));

		btnRun = new JButton("Run Algorithm");
		btnRun.addActionListener(new RunAlgorithmActionListener());
		GridBagConstraints gbc_btnRun = new GridBagConstraints();
		gbc_btnRun.gridwidth = 2;
		gbc_btnRun.insets = new Insets(0, 0, 5, 5);
		gbc_btnRun.fill = GridBagConstraints.BOTH;
		gbc_btnRun.gridx = 3;
		gbc_btnRun.gridy = 8;
		gcdAssociationPanel.add(btnRun, gbc_btnRun);

		progressBar = new JProgressBar();
		GridBagConstraints gbc_progressBar = new GridBagConstraints();
		gbc_progressBar.insets = new Insets(0, 0, 5, 5);
		gbc_progressBar.fill = GridBagConstraints.BOTH;
		gbc_progressBar.gridwidth = 3;
		gbc_progressBar.gridx = 1;
		gbc_progressBar.gridy = 9;
		gcdAssociationPanel.add(progressBar, gbc_progressBar);

		stopExecution = new JButton("");
		stopExecution.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (worker != null && worker.isExecuting()) {
					worker.cancel(true);
				}
			}
		});
		stopExecution
				.setIcon(new ImageIcon(MainWindow.class.getResource("/org/datamining/associationrules/ui/stop.gif")));
		GridBagConstraints gbc_stopExecution = new GridBagConstraints();
		gbc_stopExecution.fill = GridBagConstraints.BOTH;
		gbc_stopExecution.insets = new Insets(0, 0, 5, 5);
		gbc_stopExecution.gridx = 4;
		gbc_stopExecution.gridy = 9;
		gcdAssociationPanel.add(stopExecution, gbc_stopExecution);

		progressLabel = new JLabel(" ");
		GridBagConstraints gbc_progressLabel = new GridBagConstraints();
		gbc_progressLabel.fill = GridBagConstraints.VERTICAL;
		gbc_progressLabel.gridwidth = 3;
		gbc_progressLabel.insets = new Insets(0, 0, 0, 5);
		gbc_progressLabel.gridx = 1;
		gbc_progressLabel.gridy = 10;
		gcdAssociationPanel.add(progressLabel, gbc_progressLabel);

		setMinimumSize(getSize());
	}

	private final class BrowseActionListener implements ActionListener {
		private JTextField txtField;
		private String description;
		private String endsWith;
		private boolean showSaveDialog;

		public BrowseActionListener(JTextField txtField, String description, String endsWith) {
			this(txtField, description, endsWith, false);
		}

		public BrowseActionListener(JTextField txtField, String description, String endsWith, boolean showSaveDialog) {
			this.txtField = txtField;
			this.description = description;
			this.endsWith = endsWith;
			this.showSaveDialog = showSaveDialog;
		}

		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fc.setFileFilter(new FileFilter() {

				@Override
				public String getDescription() {
					return description;
				}

				@Override
				public boolean accept(File f) {
					return f.isDirectory() || f.getName().endsWith(endsWith);
				}
			});
			int choice = showSaveDialog ? fc.showSaveDialog(MainWindow.this) : fc.showOpenDialog(MainWindow.this);
			if (choice != JFileChooser.APPROVE_OPTION) {
				return;
			}
			String path = fc.getSelectedFile().getPath();
			txtField.setText(path);
		}
	}

	private PrintStream outBackup;
	private JButton stopExecution;
	public RunnerWorker worker;
	private JLabel lblMinimumConfidence;
	private JSpinner spinnerConfidence;
	private JLabel lblMaximumCombinationsLimit;
	private JSpinner spinnerCombinationsLimit;

	private final class RunAlgorithmActionListener implements ActionListener {

		private void updateTextArea(final String text) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					display.append(text);
				}
			});
		}

		public void actionPerformed(ActionEvent e) {
			preProcessing();
			worker = new RunnerWorker();
			SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss a");
			final String started = formatter.format(Calendar.getInstance().getTime());
			progressLabel.setText("Started at " + started + "    00%");
			progressBar.setValue(0);
			worker.addPropertyChangeListener(new PropertyChangeListener() {
				@Override
				public void propertyChange(PropertyChangeEvent e) {
					if ("progress".equals(e.getPropertyName())) {
						progressBar.setIndeterminate(false);
						int newValue = (Integer) e.getNewValue();
						String valueString = (newValue < 10) ? "0" + newValue : "" + newValue;
						progressLabel.setText("Started at " + started + "    " + valueString + "%");
						progressBar.setValue(newValue);
					}
				}
			});
			worker.execute();
		}

		private void preProcessing() {
			btnRun.setEnabled(false);
			display.setText("");
			outBackup = System.out;
			OutputStream out = new OutputStream() {
				@Override
				public void write(int b) throws IOException {
					updateTextArea(String.valueOf((char) b));
					flush();
				}

				@Override
				public void write(byte[] b, int off, int len) throws IOException {
					updateTextArea(new String(b, off, len));
					flush();
				}

				@Override
				public void write(byte[] b) throws IOException {
					write(b, 0, b.length);
				}
			};

			System.setOut(new PrintStream(out, true));
		}
	}

	public final class RunnerWorker extends SwingWorker<Integer, Double> {

		public boolean isExecuting() {
			return isActive();
		}

		@Override
		protected Integer doInBackground() throws Exception {
			try {
				GCDAssociationRules.setWorker(this);
				String inputPath = txtInput.getText();
				String outputPath = txtOutput.getText();
				String inputFrequency = spinnerSupport.getValue().toString();
				String inputConfThreshold = spinnerConfidence.getValue().toString();
				String maximumCombinationsLimit = spinnerCombinationsLimit.getValue().toString();
				GCDAssociationRules.main(new String[] { inputPath, outputPath, inputFrequency, inputConfThreshold,
						maximumCombinationsLimit });
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(MainWindow.this, e1.getClass().getName() + ": " + e1.getMessage(),
						"Generation Failed", JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			}
			return 0;
		}

		public void mySetProgress(int x) {
			setProgress(x);
		}

		@Override
		protected void done() {
			btnRun.setEnabled(true);
			System.setOut(outBackup);
		}
	}
}
