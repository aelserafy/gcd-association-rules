/**
 * Warranty disclaimer: This software is provided 'as-is', without any express or implied warranty.    
 * In no event will the author(s) be held liable for any damages arising from the use of this software.  
 * 
 * Copyrights (c) 2015 to Ahmed El-Serafy (a.elserafy@ieee.org) and Hazem El-Raffiee (hazem.farouk.elraffiee@gmail.com)  
 * 
 * All of the files that are part of the GCDs Association Rules algorithm are licensed under either GPL v.3 or dual-licensed under the following terms.  
 * 
 * 1- Any use of the provided source code must be preceded by a written authorization from one of the author(s).  
 * 2- The license text must be kept in source files headers.   
 * 3- The use of the provided source code must be acknowledged in the project documentation and any consequent presentations or documents.   
 * This is achieved by referring to the original repository (https://bitbucket.org/aelserafy/gcd-association-rules)  
 * 4- Any enhancements introduced to the provided algorithm must be shared with the original author(s) along with its source code and changes log.   
 * This is if you are building directly or indirectly upon the algorithm provided by the original author(s).  
 * 5- The public availability of the new source code is provided upon agreement with the original author(s).  
 * 6- For commercial distribution and use, a license agreement must be obtained from one of the author(s).  
*/

package org.datamining.associationrules;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.datamining.associationrules.ui.MainWindow.RunnerWorker;

public class GCDAssociationRules {
	private static final String H = "%dh";
	private static final String M = "%dm";
	private static final String S = "%ds";
	private static final String MS = "%dms";
	private static final String COLON = ":";
	private static final DateFormat FORMATTER = new SimpleDateFormat("[hh:mm:ss a]");
	private static RunnerWorker worker;
	private static long start;
	private DatTransformer datTransformer;
	private double inputConfThreshold;
	private double inputFreqThreshold;
	private String outputFilePath;
	private String inputFilePath;

	public static void main(String[] args) throws Exception {
		System.out.println(Arrays.toString(args));
		String inputFilePath = args[0];
		String outputFilePath = args[1];
		double inputFreqThreshold = Double.parseDouble(args[2]);
		double inputConfThreshold = Double.parseDouble(args[3]);
		int combinationsElementsLimit = Integer.parseInt(args[4]);
		GCDAssociationRules gcdRunner = new GCDAssociationRules(inputFilePath, outputFilePath, inputFreqThreshold,
				inputConfThreshold, combinationsElementsLimit);

		start = Calendar.getInstance().getTimeInMillis();
		gcdRunner.runAlgorithm();
		long end = Calendar.getInstance().getTimeInMillis();
		long ms = end - start;
		System.out.println("Total in milliseconds: " + ms);
		System.out.println("Formated total: " + formatDuration(ms));
	}

	public static String formatDuration(long total_milliseconds) {
		long total_seconds = total_milliseconds / 1000;
		long hours = total_seconds / 3600;
		long minutes = (total_seconds % 3600) / 60;
		long seconds = total_seconds % 60;
		long milliseconds = total_milliseconds - (total_seconds * 1000);
		String formatted_number;
		if (hours > 0) {
			formatted_number = String.format(H + COLON + M + COLON + S + COLON + MS, hours, minutes, seconds,
					milliseconds);
		} else if (minutes > 0) {
			formatted_number = String.format(M + COLON + S + COLON + MS, minutes, seconds, milliseconds);
		} else if (seconds > 0) {
			formatted_number = String.format(S + COLON + MS, seconds, milliseconds);
		} else {
			formatted_number = String.format(MS, milliseconds);
		}

		return formatted_number;
	}

	public GCDAssociationRules(String inputFilePath, String outputFilePath, double inputFreqThreshold,
			double inputConfThreshold, int combinationsElementsLimit) {
		this.inputFilePath = inputFilePath;
		this.outputFilePath = outputFilePath;
		this.inputFreqThreshold = inputFreqThreshold;
		this.inputConfThreshold = inputConfThreshold;
		MyBigInteger.setCombinationsElementsLimit(combinationsElementsLimit);
	}

	private void runAlgorithm() throws Exception {
		List<String> inputLines = extractInputFileLines();

		// Read the file and assign primes based on the frequency of items in
		// the input file
		// Constructs the transactions set, removes duplicate transactions and
		// calculates the multiplications
		datTransformer = new DatTransformer(inputLines, inputFreqThreshold);
		Map<Integer, List<Transaction>> transactionSets = datTransformer.getTransactionSets();
		LinkedList<Thread> allThreads = new LinkedList<Thread>();

		// Extract GCDs between entries' multiplications
		System.out.println("Extracting GCDs");
		for (Integer primeNumber : new TreeSet<Integer>(transactionSets.keySet())) {
			allThreads.addAll(new GCDAssociator(primeNumber, transactionSets).generateGCDTables());
		}
		executeThreads(allThreads);
		if (worker != null && worker.isCancelled())
			return;
		long gcdsExtractionTime = Calendar.getInstance().getTimeInMillis() - start;
		System.out.println("GCDs extraction time (ms): " + gcdsExtractionTime);

		// Extracts the support values of GCDs from the original transactions
		System.out.println("Extracting support values");
		allThreads.addAll(new GCDAssociator(0, transactionSets).getSupportCalculationThreads());
		executeThreads(allThreads);
		if (worker != null && worker.isCancelled())
			return;
		long supportCalculationTime = Calendar.getInstance().getTimeInMillis() - start - gcdsExtractionTime;
		System.out.println("Support values time (ms): " + supportCalculationTime);

		// Cleaning up some data and signaling the GC
		new GCDAssociator(0, transactionSets).cleanUp();

		// Calculating confidence values and constructing the association rules
		// objects
		System.out.println("Extracting confidence values");
		allThreads.addAll(new GCDAssociator(0, null).getConfidenceCalculationThreads(datTransformer.getFreqThreshold(),
				inputConfThreshold));
		executeThreads(allThreads);
		if (worker != null && worker.isCancelled())
			return;
		System.out.println("Confidence values time (ms): "
				+ (Calendar.getInstance().getTimeInMillis() - start - gcdsExtractionTime - supportCalculationTime));

		// Sorting association rules according to their support values
		List<AssociationRule> associationRules = GCDAssociator.getAssociationRules();
		AssociationRule.setTotalTransactionsCount(datTransformer.getRowsCount());
		Collections.sort(associationRules, new Comparator<AssociationRule>() {
			@Override
			public int compare(AssociationRule o1, AssociationRule o2) {
				return o2.getSupport().compareTo(o1.getSupport());
			}
		});

		List<String> lines = constructCSVLines(associationRules);

		writeCSV(lines);

		GCDAssociator.getResults().clear();
		associationRules.clear();
		lines.clear();
		datTransformer.cleanUP();
		System.gc();
	}

	private List<String> constructCSVLines(List<AssociationRule> associationRules) {
		List<String> lines = new LinkedList<String>();
		for (AssociationRule rule : associationRules) {
			StringBuilder line = new StringBuilder();
			line.append(getOriginalFactorsForGCD(rule.getAntecedent())).append(",");
			line.append(getOriginalFactorsForGCD(rule.getConsequent())).append(",");
			double support = rule.getSupport();
			line.append((int) support).append(",");
			line.append(support / AssociationRule.getTotalTransactionsCount()).append(",");
			line.append(rule.getConfidence());
			lines.add(line.toString());
		}
		return lines;
	}

	private List<String> extractInputFileLines() throws FileNotFoundException, IOException {
		List<String> inputLines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(new File(inputFilePath)));
		String line;
		while ((line = reader.readLine()) != null)
			inputLines.add(line);
		reader.close();
		return inputLines;
	}

	private String getOriginalFactorsForGCD(MyBigInteger gcd) {
		Integer[] originalFactors = datTransformer.primes2originals(gcd.getFactors());
		Arrays.sort(originalFactors);
		String factors = "\"" + Arrays.toString(originalFactors) + "\"";
		return factors.replaceAll("[\\[\\]]", "");
	}

	private void writeCSV(List<String> lines) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(outputFilePath)));
		writer.append("Antecedent,Consequent,Support,Support %,Confidence %\n");
		for (String line : lines) {
			writer.append(line).append("\n");
		}
		writer.close();
	}

	/**
	 * This algorithm can be parallelized in many of its phases. So this
	 * function is made to utilize the processing power of multiple cores by
	 * maintaining a constant number of threads that equals the number of cores
	 */
	private void executeThreads(LinkedList<Thread> threads) throws InterruptedException {
		int threadsCount = threads.size();
		if (threadsCount == 0)
			return;
		int max_running_threads = (Runtime.getRuntime().availableProcessors());
		Thread[] activeThreads = new Thread[max_running_threads];
		for (int i = 0; i < activeThreads.length; ++i) {
			activeThreads[i] = threads.poll();
			if (activeThreads[i] == null)
				break;
			activeThreads[i].start();
		}
		int progress = calculateProgress(threads, threadsCount, max_running_threads);
		if (worker != null) {
			worker.mySetProgress(progress);
			if (worker.isCancelled()) {
				return;
			}
		}
		printProgress(progress);
		Thread nextThread = threads.poll();
		long time = Calendar.getInstance().getTimeInMillis();
		while (nextThread != null) {
			int i;
			for (i = 0; activeThreads[i].isAlive(); i = (i + 1) % activeThreads.length)
				;
			activeThreads[i] = nextThread;
			activeThreads[i].start();
			progress = calculateProgress(threads, threadsCount, max_running_threads);
			if (worker != null) {
				worker.mySetProgress(progress);
				if (worker.isCancelled()) {
					return;
				}
			}
			nextThread = threads.poll();
			if (Calendar.getInstance().getTimeInMillis() - time > 5000) {
				printProgress(progress);
				time = Calendar.getInstance().getTimeInMillis();
				// System.gc();
			}
		}
		for (int i = 0; i < activeThreads.length; i = (i + 1) % activeThreads.length) {
			if (activeThreads[i] == null) {
				continue;
			}
			if (activeThreads[i].isAlive()) {
				continue;
			}
			int doneCounter = 0;
			for (int j = 0; j < activeThreads.length; ++j) {
				if (activeThreads[j] != null && !activeThreads[j].isAlive()) {
					activeThreads[j] = null;
				}
				if (activeThreads[j] == null) {
					++doneCounter;
				}
			}
			progress = Math.round(100 * ((threadsCount - max_running_threads + doneCounter) / (float) threadsCount));
			if (worker != null) {
				worker.mySetProgress(progress);
				if (worker.isCancelled()) {
					return;
				}
			}
			printProgress(progress);
			if (doneCounter == max_running_threads) {
				break;
			}
		}
		if (worker != null) {
			worker.mySetProgress(100);
			printProgress(100);
		}
	}

	private int calculateProgress(LinkedList<Thread> threads, int threadsCount, int max_running_threads) {
		return Math.round(100 * ((threadsCount - threads.size() - max_running_threads) / (float) threadsCount));
	}

	private void printProgress(int progress) {
		String time = FORMATTER.format(Calendar.getInstance().getTime());
		System.out.println(time + " " + progress + "%");
	}

	public static void setWorker(RunnerWorker worker) {
		GCDAssociationRules.worker = worker;
	}
}
