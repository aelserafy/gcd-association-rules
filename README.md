# GCD Association Rules v.1.2 #
  
This application finds the association rules in a given sequence of transactions/group of events. It takes an input file of sequence of events in a space separated format, while each line represents an entry. Example datasets are included in a directory called "Datasets". The output is a set of association rules sorted by support in a descending order. Where an association rule X==>Y is a relationship between two itemsets (sets of items/events) X and Y such that the intersection of X and Y is empty. The support of a rule is the number of transactions that contains X∪Y divided by the total number of transactions. The confidence of a rule is the number of transactions that contains X∪Y divided by the number of transactions that contain X.  
  
Algorithm parameters: Minimum support, Minimum confidence, Maximum combinations limit  
Where the maximum combinations limit is an integer that starts from 1 and limits the combinations generated between the common factors of two entries. Increasing this parameter increases the algorithm's complexity exponentially, so reasonable values for this parameter is between 1 and 4.  
  
### This algorithm operates in the following steps: ###
**1- Input file pre-processing**  
1.a- A first pass on the input file to sort the events according to their frequency  
1.b- A second pass to replace the most frequent events with the smallest not yet used prime, starting from 2. This replacement stops at the minimum support percentage. The multiplication of the replaced primes is calculated per entry  
1.c- Repeated multiplications are eliminated while being represented by a single entry that holds the repetition count  
  
**2- Processing the input data**  
2.a- The input data are divided into sets according to the most frequent event in each entry. This means that we'll have a set for entries starting with 2, a set for entries starting with 3... etc  
2.b- The GCD is calculated between each entry's multiplication result and the other entries in the same set, as well as, all the entries from the other sets. Beside calculating the GCDs, common factors between these entries are combined up to the maximum combination limit and appended to the GCDs list. This step utilizes threading per entry.  
2.c- The support of each found GCD from (2.b) is extracted by checking if each transaction's multiplication from the original set is divisible by this GCD  
2.d- GCDs are sorted in a descending order according to support values while not considering anything below the minimum support  
2.e- Confidence values are calculated while skipping some processing when confidence values drop below the specified minimum  
  
**3- Association rules output file**  
3.a- Factorize each GCD to its original factors and reverse the translation made in step 1.b. By now, you should realize why we used primes... :)  
3.b- Print the original events to the output file along with the collected support and confidence values of the association rules  
  
Note: This implementation contains lots of tweaks that utilize bits and pieces from several data structures and algorithms to optimize it for size and performance. Feel free to explore the code yourself, or ask any of the authors included in the license file...  
  
### How to use? ###
**Usage 1**: Run the UI, you should know how to use it...  
  
**Usage 2**: java -jar gcd_association_rules.jar input.dat output.csv minSupp minConf maxComb  
  
**Command line example**: java -jar gcd_association_rules.jar Datasets/letRecog.D106.N20000.C26.num Datasets/letRecog.D106.N20000.C26.csv 0.3 0.75  2  
Where, 0.3 means that the minimum support is 30%, 0.75 means that the minimum confidence is 75%  and 2 means that the maximum combinations limit is 2